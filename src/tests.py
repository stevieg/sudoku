#!/usr/bin/env python

import sudoku
import collections
import unittest

# Test grids
test_grids = collections.OrderedDict()

# Simple
t1 = [[0,6,0,0,0,0,8,9,0],
      [7,9,0,0,0,0,0,0,0],
      [0,0,3,4,0,0,1,0,7],
      [0,7,0,5,2,8,0,0,0],
      [3,0,2,0,0,7,0,8,0],
      [0,1,0,3,9,4,0,0,0],
      [0,0,1,7,0,0,2,0,9],
      [2,8,0,0,0,0,0,0,0],
      [0,4,0,0,0,0,3,7,0]]
test_grids["Simple"] = t1

# Medium
t2 = [[0,0,9,0,0,0,0,0,0],
      [5,7,0,0,0,0,2,0,0],
      [3,0,0,5,1,2,0,8,0],
      [0,4,7,0,0,0,3,0,0],
      [1,0,5,7,0,0,4,0,0],
      [0,0,0,0,8,0,6,0,0],
      [0,0,0,0,3,5,0,0,7],
      [0,0,0,0,0,6,0,4,0],
      [0,0,0,0,4,0,5,2,0]]
test_grids["Medium"] = t2

# Hard
t3 = [[0,0,0,0,3,2,0,0,0],
      [0,0,2,0,0,0,0,7,0],
      [0,4,0,6,5,0,0,0,0],
      [1,0,0,0,4,0,0,0,3],
      [2,0,0,8,0,3,1,0,6],
      [0,0,0,0,6,0,4,0,0],
      [8,2,0,0,0,0,0,4,0],
      [4,0,7,0,0,0,9,0,0],
      [0,9,5,0,8,7,0,0,0]]
test_grids["Hard"] = t3

# World Championship
t4 = [[0,5,0,0,2,0,0,3,0],
      [2,0,0,0,0,1,7,0,8],
      [4,0,7,6,0,0,0,0,0],
      [0,0,0,0,0,5,0,0,0],
      [5,2,0,0,0,0,0,4,7],
      [0,0,0,7,0,0,0,0,0],
      [0,0,0,0,0,3,5,0,4],
      [3,0,6,5,0,0,0,0,1],
      [0,9,0,0,7,0,0,6,0]]
test_grids["World Championship"] = t4

# Shortz 301
t5 = [[0,3,9,5,0,0,0,0,0],
      [0,0,0,8,0,0,0,7,0],
      [0,0,0,0,1,0,9,0,4],
      [1,0,0,4,0,0,0,0,3],
      [0,0,0,0,0,0,0,0,0],
      [0,0,7,0,0,0,8,6,0],
      [0,0,6,7,0,8,2,0,0],
      [0,1,0,0,9,0,0,0,5],
      [0,0,0,0,0,1,0,0,8]]
test_grids["Shortz 301"] =t5

# Mepham diabolical
t6 = [[0,9,0,7,0,0,8,6,0],
      [0,3,1,0,0,5,0,2,0],
      [8,0,6,0,0,0,0,0,0],
      [0,0,7,0,5,0,0,0,6],
      [0,0,0,3,0,7,0,0,0],
      [5,0,0,0,1,0,7,0,0],
      [0,0,0,0,0,0,1,0,9],
      [0,2,0,6,0,0,3,5,0],
      [0,5,4,0,0,8,0,7,0]]
test_grids["Mepham diabolical"] = t6

# Super-hard
t7 = [[0,0,0,0,3,7,6,0,0],
      [0,0,0,6,0,0,0,9,0],
      [0,0,8,0,0,0,0,0,4],
      [0,9,0,0,0,0,0,0,1],
      [6,0,0,0,0,0,0,0,9],
      [3,0,0,0,0,0,0,4,0],
      [7,0,0,0,0,0,8,0,0],
      [0,1,0,0,0,9,0,0,0],
      [0,0,2,5,4,0,0,0,0]]
test_grids["Super-hard"] = t7

# Empty
t8 = [[0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0,0]]
test_grids["Empty"] = t8

class TestSudoku(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.dim = 3

    def test_index_vs_address(self):
        s = sudoku.Sudoku(TestSudoku.dim)
        i = 0
        for r in range(TestSudoku.dim**2):
            for c in range(TestSudoku.dim**2):
                self.assertEqual(s._index_from_address(r+1,c+1),i)
                self.assertEqual((r+1,c+1),s._address_from_index(i))
                i = i+1

    def test_indexes_for_column(self):
        s = sudoku.Sudoku(TestSudoku.dim)
        self.assertEqual(s._indexes_for_column(1),    [0, 9, 18, 27, 36, 45, 54, 63, 72])
        self.assertEqual(s._indexes_for_row(1),       [0, 1, 2, 3, 4, 5, 6, 7, 8])
        self.assertEqual(s._indexes_for_column(5),    [4, 13, 22, 31, 40, 49, 58, 67, 76])
        self.assertEqual(s._indexes_for_row(4),       [27, 28, 29, 30, 31, 32, 33, 34, 35])
        self.assertEqual(s._subgrid_from_address(1,1),(1, 1))
        self.assertEqual(s._subgrid_from_address(9,7),(3, 3))
        self.assertEqual(s._subgrid_from_address(6,7),(2, 3))
        self.assertEqual(s._indexes_for_subgrid(1,1), [0, 1, 2, 9, 10, 11, 18, 19, 20])
        self.assertEqual(s._indexes_for_subgrid(3,3), [60, 61, 62, 69, 70, 71, 78, 79, 80])
        self.assertEqual(s._indexes_for_subgrid(2,3), [33, 34, 35, 42, 43, 44, 51, 52, 53])

    def test_invalid_values(self):
        s = sudoku.Sudoku(TestSudoku.dim)
        self.assertRaises(ValueError,s.set_cell,2,2,10)
        self.assertRaises(ValueError,s.set_cell,10,1,1)
        self.assertRaises(ValueError,s.set_cell,1,10,1)
        self.assertRaises(ValueError,s.set_cell,0,1,1)
        self.assertRaises(ValueError,s.set_cell,1,0,1)

    def test_solve(self):
        s = sudoku.Sudoku(TestSudoku.dim)
        self.assertTrue(sudoku.Sudoku().set_from_list_of_lists(test_grids["Simple"]).solve())
        self.assertTrue(sudoku.Sudoku().set_from_list_of_lists(test_grids["Medium"]).solve())
        self.assertTrue(sudoku.Sudoku().set_from_list_of_lists(test_grids["Hard"]).solve())
        self.assertTrue(sudoku.Sudoku().set_from_list_of_lists(test_grids["World Championship"]).solve())
        self.assertTrue(sudoku.Sudoku().set_from_list_of_lists(test_grids["Shortz 301"]).solve())
        self.assertTrue(sudoku.Sudoku().set_from_list_of_lists(test_grids["Mepham diabolical"]).solve())

# Execute unit tests
if __name__ == "__main__":
    unittest.main()
