#!/usr/bin/env python

# Class to model a Sudoku grid and to try to solve incomplete grids by inferring values
# for empty cells using a process of elimination
# (c) 2016 Steven Good

import collections
import copy

class Sudoku:

    def __init__(self,dim=3):
        self._dim = dim
        self._grid = [0 for i in range(self._dim**4)]
        self._reset_possible()

    def __str__(self):
        sl = []
        hbreak = (("+"+("----"*self._dim))*self._dim)+"+\n"
        sl.append(hbreak)
        for r in range(self._dim**2):
            sl.append("|")
            for c in range(self._dim**2):
                if self.get_cell(r+1,c+1)==0:
                    sl.append("    ")
                else:
                    sl.append(" %2d " % (self.get_cell(r+1,c+1)))
                if (c+1)%self._dim==0:
                    sl.append("|")
            sl.append("\n")
            if (r+1)%self._dim==0:
                sl.append(hbreak)
        return "".join(sl)

    def clone(self):
        return copy.deepcopy(self)

    def copy_from(self,another):
        self.set_from_list(another._grid)

    def get_dim(self):
        return self._dim

    def get_cell(self,r,c):
        self._validate_address(r,c)
        return self._grid[self._index_from_address(r,c)]

    def set_from_list(self,l):
        if len(l)!=(self._dim**4):
           raise ValueError("list should contain %d elements for dimension=%d" % (self._dim**4,self._dim))
        for i in l:
            self._validate_value(i)
        self._grid = [i for i in l]
        self._reset_possible()
        self._infer_possible()
        return self

    def set_from_list_of_lists(self,ll):
        l = [i for sl in ll for i in sl]
        self.set_from_list(l)
        return self

    def set_cell(self,r,c,n):
        self._validate_address(r,c)
        self._validate_value(n)
        i = self._index_from_address(r,c)
        if self._grid[i]!=0:
            self.clear_cell(r,c)
        self._grid[i] = n
        #self._reset_possible()
        self._infer_possible()
        return self

    def clear_cell(self,r,c):
        self._validate_address(r,c)
        i = self._index_from_address(r,c)
        self._grid[i] = 0
        self._reset_possible()
        self._infer_possible()
        return self

    def solve(self,debug=False):
        solution = self._solve(0,debug)
        if solution != None:
            if debug:
                print solution
            self.copy_from(solution)
        return self._score() == self._dim**4

    def _solve(self,depth=0,debug=False):
        if depth == 0:
            self._reset_possible()
        self._infer_possible()
        score = self._score()
        target = self._dim**4
        while True:
            self._assign_possible()
            nscore = self._score()
            if nscore == score:
                # No improvement on this loop so break out
                break
            else:
                score = nscore
        # Finally, we need to "deep solve" any remaining gaps
        # Attempt to recursively solve by iterating through possible solutions
        # We effectively create copies of this object, filling in a cell, then recurse
        if debug:
            print "%s_solve: depth=%d, score=%d" % ("\t"*depth,depth,self._score())
            if depth == 0:
                print "_solve: commencing deep solve phase from this point:"
                print self
        # Rather than ierate through each cell in grid order, we will instead try
        # setting the cells starting from the ones with the fewest remaining valid
        # possibilities, in the hope of pruning the search tree
        cells_by_possibilities = [[] for i in range(self._dim**2+1)]
        for i in range(self._dim**4):
            cells_by_possibilities[len(self._possible[i])].append(i)
        for l in range(1,len(cells_by_possibilities)):
            for i in cells_by_possibilities[l]:
                if len(self._possible[i])>0:
                    for p in self._possible[i]:
                        if debug:
                            print "%s_solve: recurse at depth=%d, index=%d, value=%d" % ("\t"*depth,depth,i,p)
                        candidate = self.clone()
                        (r,c) = self._address_from_index(i)
                        candidate.set_cell(r,c,p)
                        result = candidate._solve(depth+1,debug)
                        if result != None:
                            if debug:
                                print "%s_solve: solution found at depth=%d, index=%d, value=%d, score=%d" % ("\t"*depth,depth,i,p,result._score())
                            # A solution has been returned, so percolate upwards
                            return result
        # We'll arrive here is there are no possibilities left to explore
        # That could be because we've got a solution, or because we've got a problem
        if self._score() == target:
            return self
        else:
            return None

    def _score(self):
        n = 0
        for i in range(self._dim**4):
            if self._grid[i]>0:
                n = n+1
        return n

    def _reset_possible(self):
        self._possible = [set(j+1 for j in range(3**2)) for i in range(self._dim**4)]

    def _infer_possible(self):
        # Iterate through each cell in the grid and eliminate any impossible
        # values for that cell from the _possible set for it.
        for i in range(self._dim**4):
            (r,c) = self._address_from_index(i)
            (gr,gc) = self._subgrid_from_address(r,c)
            # Clear possibilities for assigned cells
            if self._grid[i]>0:
                self._possible[i].clear()
                continue
            # Eliminate any non-zero values in the same row
            for j in self._indexes_for_row(r):
                self._possible[i].discard(self._grid[j])
            # Eliminate any non-zero values in the same column
            for j in self._indexes_for_column(c):
                self._possible[i].discard(self._grid[j])
            # Eliminate any non-zero values in the same subgrid
            for j in self._indexes_for_subgrid(gr,gc):
                self._possible[i].discard(self._grid[j])

    def _assign_possible(self):
        # First the simple case where we have inferred a single possibility for the cell
        for i in range(self._dim**4):
            if len(self._possible[i])==1:
                (r,c) = self._address_from_index(i)
                self.set_cell(r,c,next(iter(self._possible[i])))
        # Now the more complex scenario
        # Give a set of cells and the possible values, what values are possible only once?
        # By row
        for n in range(self._dim**2):
            il = self._indexes_for_row(n+1)
            vl = self._find_single_possibilities(il)
            for v in vl:
                for i in il:
                    if v in self._possible[i]:
                        (r,c) = self._address_from_index(i)
                        self.set_cell(r,c,v)
        # By column
        for n in range(self._dim**2):
            il = self._indexes_for_column(n+1)
            vl = self._find_single_possibilities(il)
            for v in vl:
                for i in il:
                    if v in self._possible[i]:
                        (r,c) = self._address_from_index(i)
                        self.set_cell(r,c,v)
        # By subgrid
        for n in range(self._dim):
            for m in range(self._dim):
                il = self._indexes_for_subgrid(n+1,m+1)
                vl = self._find_single_possibilities(il)
                for v in vl:
                    for i in il:
                        if v in self._possible[i]:
                            (r,c) = self._address_from_index(i)
                            self.set_cell(r,c,v)

    def _find_single_possibilities(self,l):
        # First count occurences of the values as possibilities for each cell
        ct = collections.defaultdict(int)
        for i in l:
            for v in range(self._dim**2):
                if (v+1) in self._possible[i]:
                    ct[v+1] = ct[v+1]+1
        # Now find values with a single occurrence
        r = [v+1 for v in range(self._dim**2) if ct[v+1]==1]
        return r

    def _validate_address(self,r,c):
        if r<1:
            raise ValueError("r must be >=1")
        elif r>self._dim**2:
            raise ValueError("r must be <=%d" % self._dim**2)
        elif c<1:
            raise ValueError("c must be >=1")
        elif c>self._dim**2:
            raise ValueError("c must be <=%d" % self._dim**2)

    def _validate_subgrid(self,gr,gc):
        if gr<1:
            raise ValueError("gr must be >=1")
        elif gr>self._dim**2:
            raise ValueError("gr must be <=%d" % self._dim)
        elif gc<1:
            raise ValueError("gc must be >=1")
        elif gc>self._dim:
            raise ValueError("gc must be <=%d" % self._dim)

    def _validate_value(self,n):
        if n<0:
            raise ValueError("value must be >=0")
        elif n>self._dim**2:
            raise ValueError("value must be <=%d" % self._dim**2)

    def _index_from_address(self,r,c):
        return ((r-1)*(self._dim**2)+(c-1))

    def _address_from_index(self,i):
        r = (i/(self._dim**2))+1
        c = (i%(self._dim**2))+1
        return (r,c)

    def _subgrid_from_address(self,r,c):
        gr = ((r-1)/self._dim)+1
        gc = ((c-1)/self._dim)+1
        return(gr,gc)

    def _indexes_for_row(self,r):
        s = [(r-1)*(self._dim**2)+i for i in range(self._dim**2)]
        return s

    def _indexes_for_column(self,c):
        s = [(c-1)+(self._dim**2)*i for i in range(self._dim**2)]
        return s

    def _indexes_for_subgrid(self,gr,gc):
        r = ((gr-1)*self._dim)+1
        c = ((gc-1)*self._dim)+1
        s = []
        for dr in range(self._dim):
            for dc in range(self._dim):
                s.append(self._index_from_address(r+dr,c+dc))
        return s
